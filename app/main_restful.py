import json
import unidecode
from flask import Flask, abort, make_response, jsonify
from flask_restful import Api, Resource, fields, marshal, reqparse

app = Flask(__name__)
api = Api(app)

with open('../Restaurantes.json','r') as f:
    restaurantes = json.load(f)

restaurant_fields = {
    'codigo': fields.String,
    'nombre': fields.String,
    'direccion': fields.String,
    'c.p.': fields.String,
    'municipio': fields.String,
    'pedania': fields.String,
    'Telefono': fields.String
}


class RestauranteAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('name', type=str, required=True,
                                   help='Restaurant name not provided', location='json')
        super(RestauranteAPI, self).__init__()

    def get(self, name):
        sanitized = name.lower()
        sanitized = sanitized.strip()
        listaNom = []
        index = -1
        found = False
        for i, key in enumerate(restaurantes):
            try:
                if (sanitized in unidecode.unidecode(key['Nombre'].lower())) or (sanitized == key['Código']):
                    found = True
                    index = i
                    listaNom.append(restaurantes[index])
            except:
                pass
            
        if found:
            return {'restaurante': listaNom}
        return make_response(jsonify({'message': 'Not found'}), 404)

class RestaurantesListAPI(Resource):
    def __init__(self):
        super(RestaurantesListAPI, self).__init__()

    def get(self):
        return {'restaurantes': restaurantes}

class MunicipioAPI(Resource):
    def get(self,municipio):
        sanitized = municipio.lower()
        sanitized = sanitized.strip()
        listaMun = []
        index = -1
        found = False
        for i, key in enumerate(restaurantes):
            try:
                if sanitized in key['Municipio'].lower():
                    found = True
                    index = i
                    listaMun.append(restaurantes[index])
            except:
                pass
            
                
        if found:
            return {'restaurante': listaMun}
        return make_response(jsonify({'message': 'Not found'}), 404)

class PedaniaAPI(Resource):
    def get(self, pedania):
        sanitized = pedania.lower()
        sanitized = sanitized.strip()
        listaPed = []
        index = -1
        found = False
        for i, key in enumerate(restaurantes):
            try:
                if (sanitized in unidecode.unidecode(key['Pedanía'].lower())):
                    found = True
                    index = i
                    listaPed.append(restaurantes[index])
            except:
                pass
        if found:
            return {'restaurante': restaurantes[index]}
        return make_response(jsonify({'message': 'Not found'}), 404)

api.add_resource(RestauranteAPI, '/restaurantes/<string:name>', endpoint = 'restaurante')
api.add_resource(RestaurantesListAPI, '/restaurantes', endpoint='restaurantes')
api.add_resource(MunicipioAPI, '/restaurantes/municipio/<string:municipio>', endpoint='municipio')
api.add_resource(PedaniaAPI, '/restaurantes/pedania/<string:pedania>',endpoint='pedania')


if __name__ == '__main__':
    app.run(debug=True)