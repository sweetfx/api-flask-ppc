import json
import codecs
import unidecode
from flask import Flask, abort, make_response, jsonify, request, render_template

app = Flask(__name__, template_folder='./templates')

with codecs.open('../restaurantes.json', 'r', 'utf-8-sig') as f:
    restaurantes = json.load(f)

codes = []

for rest in restaurantes:
    codes.append(int(rest['Código']))

codes.sort()


def sanitize_str(string):
    sanitized = string.lower()
    sanitized = sanitized.strip()
    return sanitized

#######################################

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

# Renderizar una página web
@app.route('/')
def index():
    return render_template('index.html', restaurantes=restaurantes)

@app.route('/<string:name>')
def get_restaurante(name):
    restaurante = get_restaurante_search('Nombre', name)['restaurante']
    return render_template('restaurante.html', restaurante=restaurante)

@app.route('/crear-restaurante')
def nuevo_restaurante():
    return render_template('formulario.html')

#######################################

# Funcionar como una API
@app.route('/api/restaurantes', methods=['GET'])
def get_restaurantes():
    return jsonify({'restaurantes': restaurantes})

@app.route('/api/restaurantes/search', methods=['GET'])
def search():
    param = request.args.get('param', type=str)
    value = request.args.get('value', type=str)

    if (param and value):
        if (param == 'name'):
            return get_restaurante_search('Nombre', int(value))
        elif (param == 'code'):
            return get_restaurante_search('Código', int(value))
        elif (param == 'municipio'):
            return get_restaurantes_search('Municipio', value)
        elif (param == 'pedania'):
            return get_restaurantes_search('Pedanía', value)
    abort(404)

def get_restaurante_search(param, value):
    sanitized = sanitize_str(value)
    restaurante = {}
    index = -1
    found = False
    for i, key in enumerate(restaurantes):
        try:
            if (sanitized == unidecode.unidecode(key[param].lower())):
                found = True
                index = i
                restaurante = restaurantes[index]
                break
        except:
            pass

    if found:
        return {'restaurante': restaurante}
    abort(404)


def get_restaurantes_search(param, value):
    sanitized = sanitize_str(value)
    listaMun = []
    index = -1
    found = False
    for i, key in enumerate(restaurantes):
        try:
            if sanitized in key[param].lower():
                found = True
                index = i
                listaMun.append(restaurantes[index])
        except:
            pass

    if found:
        return {'restaurante': listaMun}
    abort(404)

@app.route('/api/restaurante', methods=['POST'])
def create_restaurante():
    if not request.json:
        abort(400)
    restaurante = {
        'Código': str(codes[-1] + 1),
        'Nombre': request.json['name'],
        'Dirección': request.json['dir'],
        'Municipio': request.json['municipio'],
        'Pedanía': request.json['pedania'],
    }
    codes.append(codes[-1] + 1)
    restaurantes.append(restaurante)
    return jsonify({'restaurante': restaurante}), 201

if __name__ == '__main__':
    app.run(debug=True)
